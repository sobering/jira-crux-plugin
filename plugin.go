package jiraplugin

import (
	"fmt"
	"log"
	"os"

	"git-lab.boldapps.net/proximity/crux/plugin"

	gojira "github.com/andygrunwald/go-jira"
	"github.com/labstack/echo"
)

const jiraBaseURL = "http://jira.boldcommerce.com:8080"

var jira *gojira.Client

// Plugin ...
func Plugin(dashboard *plugin.Crux) {

	// Do setup and initialization of resources here.
	jira = getAuthdClient()

	dashboard.Router.Get("/jira/projects", projectsRefresh)
}

func getAuthdClient() *gojira.Client {

	jiraClient, err := gojira.NewClient(nil, jiraBaseURL)
	if err != nil {
		log.Fatal(err)
	}

	_, err = jiraClient.Authentication.AcquireSessionCookie(os.Getenv("JIRA_USER"), os.Getenv("JIRA_PASS"))
	if err != nil {
		log.Fatal(err)
	}

	return jiraClient
}

func projectsRefresh(c *echo.Context) error {
	c.JSON(200, project())

	return nil
}

func project() *gojira.Project {
	var projectResp *gojira.Project

	req, _ := jira.NewRequest("GET", "/rest/api/2/project/"+fmt.Sprintf("%v", os.Getenv("JIRA_PROJECT_ID")), nil)

	_, err := jira.Do(req, &projectResp)
	if err != nil {
		log.Fatal(err)
	}

	return projectResp
}
